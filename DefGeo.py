#!/usr/bin/python

import sys

def usage():
    print '+------------------------------------------------------------+'
    print '|            Alya geometry generator from displacement       |'
    print '|                         | BSC |                            |'
    print '| Usage:                                                     |'
    print '|       ./command <Alya geo.dat file>                        |'
    print '|          <Ensight displacement file>  <Destination file>   |'
    print '|          <scale>                                           |'
    print '|                                                            |'
    print '| NOTES:                                                     |'
    print '|      * The ensight file must not have the header.          |'
    print '|      * The scale is just in case you use <scale in Alya>   |'
    print '| if you didn\'t used scale, put \"1\"                          |'
    print '|      * This script also generates two more files to use    |'
    print '| with matia\'s Alya2VTK script.                             |'
    print '|                                                            |'
    print '|This shit is not workin\'?! Write me a line:                 |'
    print '|Alfonso Santiago - alfonso.santiago@bsc.es                  |'
    print '|Ultra beta version.                                         |'
    print '+------------------------------------------------------------+'
    sys.exit()

#f = open("geometria.geo.dat.ORIG")
#fw = open ("geometria.geo.dat.MODIF", "w")


if(len(sys.argv)< 2):
    usage()
elif(sys.argv[1]=='-v'):
    usage()

scale=float(sys.argv[4])
print 'Escala: ', scale

fa = open(sys.argv[1], 'r')
fe = open(sys.argv[2], 'r')
fw = open(sys.argv[3], 'w')

fnod= open("NODES",'w')
fele= open("ELEM",'w')

FlagNodesPerElement=False
FlagElements=False
FlagCoordinates=False
FlagBoundaries=False
FlagSkewSystems=False
FlagModif=False

NumNodes=0
CoordNod=[]
EleNod=[]
new=[0, 0, 0, 0]

print 'Reading Alya file...'
#--------------------------INICIO LECTURA DEL ARCHIVO de ALYA-------------------------
for line in fa:
    line = line.strip()
    if (line.startswith('END_')):
        FlagNodesPerElement=False
        FlagElements=False
        FlagCoordinates=False
        FlagBoundaries=False
        FlagSkewSystems=False

    elif (line.startswith('NODES_PER') or FlagNodesPerElement):
        FlagNodesPerElement=True

    elif (line.startswith('ELEMENTS') or FlagElements):
        FlagElements=True
        line=line.split()
        if line[0]!='ELEMENTS':
            new=[int(line[1]), int(line[2]),int(line[3]),int(line[4])]
            new.sort()
            if (new[3]>NumNodes):
                NumNodes=new[3]


    elif (line.startswith('COORDINATES') or FlagCoordinates):
        FlagCoordinates=True

        if (line.startswith('COORD')):
            CoordNod=[[0.0 for x in xrange(3)]for x in xrange(NumNodes+1)]

        if not(line.startswith('COORD')):
           line=line.split()
           CoordNod[int(line[0])][0]=float(line[1])*scale
           CoordNod[int(line[0])][1]=float(line[2])*scale
           CoordNod[int(line[0])][2]=float(line[3])*scale
#           print int(line[0]), CoordNod[int(line[0])]

#           if (int(line[0])==1):
#                print 'Nodo 1:', CoordNod[1]

    elif (line.startswith('BOUNDARIES') or FlagBoundaries):
        FlagBoundaries=True

    elif (line.startswith('SKEW_') or FlagSkewSystems):
        FlagSkewSystems=True

    else:
       print 'La linea leida no esta entre las opciones:'
       print line

#----------------------------FIN LECTURA DEL ARCHIVO----------------------------------
fa.close()
fa = open(sys.argv[1], 'r')

print 'Reading the ensight file...'
#--------------------------INICIO LECTURA DEL ARCHIVO de ENSIGHT-----------------------
LineCounter=1
AxisCounter=0



for line in fe:
    line = line.strip()
    if (line.startswith('Alya')):
        print 'Header of the file must be removed!'
        sys.exit()

#    print 'Nodo: ', LineCounter
#    print 'Eje: ', AxisCounter
#    print 'Valor Original', CoordNod[LineCounter][AxisCounter]
#    print 'Valor diferencia:', line 

    CoordNod[LineCounter][AxisCounter]=CoordNod[LineCounter][AxisCounter]+float(line)
   
#    print 'Resultau:', CoordNod[LineCounter][AxisCounter]
 
    LineCounter=LineCounter+1
    if (LineCounter==(NumNodes+1)):
        AxisCounter=AxisCounter+1
        LineCounter=1


#    AxisCounter=AxisCounter+1
#    if(AxisCounter==3):
#        AxisCounter=0
#        LineCounter=LineCounter+1


    

#----------------------------FIN LECTURA DEL ARCHIVO----------------------------------

print 'Writing the new file...'
#--------------------------INICIO ESCRITURA DEL ARCHIVO-------------------------------

FlagNodesPerElement=False
FlagElements=False
FlagCoordinates=False
FlagBoundaries=False
FlagSkewSystems=False

for line in fa:
    line = line.strip()
    if (line.startswith('END_')):
        FlagNodesPerElement=False
        FlagElements=False
        FlagCoordinates=False
        FlagBoundaries=False
        FlagSkewSystems=False
        fw.write(line)
        fw.write('\n')

    elif (line.startswith('NODES_PER') or FlagNodesPerElement):
        FlagNodesPerElement=True
        fw.write(line)
        fw.write('\n')

    elif (line.startswith('ELEMENTS') or FlagElements):
        fw.write(line)
        fw.write('\n')
        
        if(FlagElements):
            fele.write(line)
            fele.write('\n')

        FlagElements=True

    elif (line.startswith('COORDINATES') or FlagCoordinates):

        if(not(FlagCoordinates)):
            fw.write("COORDINATES")
            fw.write('\n')

            for x in xrange(NumNodes):
                fw.write(str(x+1))
                fw.write(" ")
                fw.write(str(CoordNod[x+1][0]))
                fw.write(" ")
                fw.write(str(CoordNod[x+1][1]))
                fw.write(" ")
                fw.write(str(CoordNod[x+1][2]))
                fw.write('\n')


                fnod.write(str(x+1))
                fnod.write(" ")
                fnod.write(str(CoordNod[x+1][0]))
                fnod.write(" ")
                fnod.write(str(CoordNod[x+1][1]))
                fnod.write(" ")
                fnod.write(str(CoordNod[x+1][2]))
                fnod.write('\n')

#            fw.write("END_COORDINATES")
#            fw.write('\n')

        FlagCoordinates=True

    elif (line.startswith('BOUNDARIES') or FlagBoundaries):
        FlagBoundaries=True
        fw.write(line)
        fw.write('\n')

    elif (line.startswith('SKEW_') or FlagSkewSystems):
        FlagSkewSystems=True
        fw.write(line)
        fw.write('\n')
    
    else:
       print 'La linea leida no esta entre las opciones:'
       print line


print 'Done!'
print 'Bye! :)'

fa.close()
fe.close()
fw.close()
