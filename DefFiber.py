#!/usr/bin/python

import sys

def usage():
    print '+------------------------------------------------------------+'
    print '|            Alya fiber generator from deformated geometry   |'
    print '|                         | BSC |                            |'
    print '| Usage:                                                     |'
    print '|       ./command <node_number> <Ensight_fiber_file>         |'
    print '|           <destination_file>                               |'
    print '|                                                            |'
    print '| NOTE:                                                      |'
    print '|      *You\'ve to remove the header of the ensight file      |'
    print '| I didn\'t had time to be so spicky with that.               |'
    print '|      * This script also generates one extra file to use    |'
    print '| with matia\'s Alya2VTK script.                              |'
    print '|                                                            |'
    print '| This buggy shit is not workin\'?! Write me a line:          |'
    print '|Alfonso Santiago - alfonso.santiago@bsc.es                  |'
    print '|Ultra beta version.                                         |'
    print '+------------------------------------------------------------+'
    sys.exit()

#f = open("geometria.geo.dat.ORIG")
#fw = open ("geometria.geo.dat.MODIF", "w")


if(len(sys.argv)< 2):
    usage()
elif(sys.argv[1]=='-v'):
    usage()

nnum=int(sys.argv[3])
print 'Number of nodes: ', nnum

#fa = open(sys.argv[1], 'r')
fe = open(sys.argv[1], 'r')
fw = open(sys.argv[2], 'w')

ffib= open("FIBERS",'w')


fibers=[]
fibers=[[0.0 for x in xrange(3)]for x in xrange(nnum+1)]



print 'Reading the ensight file...'
#--------------------------INICIO LECTURA DEL ARCHIVO de ENSIGHT-----------------------
LineCounter=1
AxisCounter=0



for line in fe:
    line = line.strip()
    if (line.startswith('Alya')):
        print 'Header of the file must be removed!'
        sys.exit()

#    print 'Nodo: ', LineCounter
#    print 'Eje: ', AxisCounter
#    print 'Valor Original', CoordNod[LineCounter][AxisCounter]
#    print 'Valor diferencia:', line 

    fibers[LineCounter][AxisCounter]=float(line)
   
#    print 'Resultau:', CoordNod[LineCounter][AxisCounter]
 
    LineCounter=LineCounter+1
    if (LineCounter==(nnum+1)):
        AxisCounter=AxisCounter+1
        LineCounter=1


#    AxisCounter=AxisCounter+1
#    if(AxisCounter==3):
#        AxisCounter=0
#        LineCounter=LineCounter+1


    

#----------------------------FIN LECTURA DEL ARCHIVO----------------------------------

print 'Writing the new file...'
#--------------------------INICIO ESCRITURA DEL ARCHIVO-------------------------------

#FlagNodesPerElement=False
#FlagElements=False
#FlagCoordinates=False
#FlagBoundaries=False
#FlagSkewSystems=False

#for line in fa:
#    line = line.strip()
#    if (line.startswith('END_')):
#        FlagNodesPerElement=False
#        FlagElements=False
#        FlagCoordinates=False
#        FlagBoundaries=False
#        FlagSkewSystems=False
#        fw.write(line)
#        fw.write('\n')

#    elif (line.startswith('NODES_PER') or FlagNodesPerElement):
#        FlagNodesPerElement=True
#        fw.write(line)
#        fw.write('\n')

#    elif (line.startswith('ELEMENTS') or FlagElements):
#        fw.write(line)
#        fw.write('\n')
        
#        if(FlagElements):
#            fele.write(line)
#            fele.write('\n')

#        FlagElements=True

#    elif (line.startswith('COORDINATES') or FlagCoordinates):

#        if(not(FlagCoordinates)):
#            fw.write("COORDINATES")
#            fw.write('\n')

for x in xrange(nnum):
    fw.write(str(x+1))
    fw.write(" ")
    fw.write(str(fibers[x+1][0]))
    fw.write(" ")
    fw.write(str(fibers[x+1][1]))
    fw.write(" ")
    fw.write(str(fibers[x+1][2]))
    fw.write('\n')


    ffib.write(str(x+1))
    ffib.write(" ")
    ffib.write(str(fibers[x+1][0]))
    ffib.write(" ")
    ffib.write(str(fibers[x+1][1]))
    ffib.write(" ")
    ffib.write(str(fibers[x+1][2]))
    ffib.write('\n')

#            fw.write("END_COORDINATES")
#            fw.write('\n')

#        FlagCoordinates=True

#    elif (line.startswith('BOUNDARIES') or FlagBoundaries):
#        FlagBoundaries=True
#        fw.write(line)
#        fw.write('\n')

#    elif (line.startswith('SKEW_') or FlagSkewSystems):
#        FlagSkewSystems=True
#        fw.write(line)
#        fw.write('\n')
    
#    else:
#       print 'La linea leida no esta entre las opciones:'
#       print line


print 'Done!'
print 'Bye! :)'

#fa.close()
fe.close()
fw.close()
